import decimal
import requests
import xml.etree.ElementTree as ET

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.dateparse import parse_date


from apps.converter.models import Rate

class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		root = ET.fromstring(requests.get('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml').text)
		counter = 0
		for child in root[2]:
			current_date = parse_date(child.attrib['time'])
			try:
				current_currency = Rate.objects.get(currency_date=current_date)
				counter += 1
			except:
				current_currency = Rate()
				current_currency.currency_date = current_date
			
			for rates in child:
				currency = rates.attrib['currency']
				rate = decimal.Decimal(rates.attrib['rate'])
				setattr(current_currency, currency, rate)
			current_currency.save()
		print('All currencies are up to date...')
				