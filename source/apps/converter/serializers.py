from rest_framework import serializers

from apps.converter.models import Rate

class RateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rate
        fields = (
        	'currency_date', 
        	'USD', 
        	'JPY', 
        	'BGN', 
        	'CZK',
        	'DKK', 
        	'GBP', 
        	'HUF', 
        	'PLN', 
        	'RON',
			'SEK',
			'CHF',
			'ISK',
			'NOK',
			'HRK',
			'RUB',
			'TRY',
			'AUD',
			'BRL',
			'CAD',
			'CNY',
			'HKD',
			'IDR',
			'ILS',
			'INR',
			'KRW',
			'MXN',
			'MYR',
			'NZD',
			'PHP',
			'SGD',
			'THB',
			'ZAR',
		)
