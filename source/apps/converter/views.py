from decimal import Decimal

from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, schema

from apps.converter.serializers import RateSerializer
from apps.converter.models import Rate


class RateViewSet(viewsets.ModelViewSet):
    queryset = Rate.objects.all().order_by('-currency_date')
    serializer_class = RateSerializer

    def get_queryset(self):
        queryset = Rate.objects.all()
        return queryset
        

@api_view(['GET',])
def convert(request):
	reference_date = request.query_params.get('reference_date', None)
	amount = request.query_params.get('amount', None)
	if amount:
		amount = Decimal(amount).quantize(Decimal('1.0000'))
	src_currency = request.query_params.get('src_currency', None)
	dest_currency = request.query_params.get('dest_currency', None)
	if reference_date:
		rates = Rate.objects.filter(currency_date=reference_date).first()
		if rates:
			initial_amount = amount
			if not src_currency == 'EUR':
				amount = Decimal(amount / getattr(rates, src_currency)).quantize(Decimal('1.0000'))
			if dest_currency == 'EUR':
				return Response({'amount': amount, 'des​t_currency': dest_currency},)
			else:
				amount = Decimal(amount * getattr(rates, dest_currency)).quantize(Decimal('1.0000'))
				return Response({'amount': amount, 'des​t_currency': dest_currency},)

	return Response(
		{
			'Usage': 'Please provide the following query params for proper usage i.e. ...convert/?amount=20&src_currency=BGN&dest_currency=EUR&reference_date=2020-04-20',
			'amount':'The amount of the currency you want to convert',
			'src_currency': 'The currency from which you will make the conversion (three upper leters)',
			'dest_currency': 'The currency you want to convert the destination currency to (three upper leters)',
			'reference_date': 'A date from the past 90 days except dates that are Saturdays or Sundays. The format is YYYY-MM-DD', 
		}
	)




