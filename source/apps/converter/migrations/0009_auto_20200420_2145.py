# Generated by Django 3.0.5 on 2020-04-20 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('converter', '0008_auto_20200420_2058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rate',
            name='currency_date',
            field=models.DateField(unique_for_date=True, verbose_name='Date'),
        ),
    ]
