from django.db import models


# Create your models here.
class Rate(models.Model):
	currency_date = models.DateField(verbose_name='Date', unique_for_date='USD')
	USD = models.DecimalField(max_digits=10, decimal_places=4)
	JPY = models.DecimalField(max_digits=10, decimal_places=4)
	BGN = models.DecimalField(max_digits=10, decimal_places=4)
	CZK = models.DecimalField(max_digits=10, decimal_places=4)
	DKK = models.DecimalField(max_digits=10, decimal_places=4)
	GBP = models.DecimalField(max_digits=10, decimal_places=4)
	HUF = models.DecimalField(max_digits=10, decimal_places=4)
	PLN = models.DecimalField(max_digits=10, decimal_places=4)
	RON = models.DecimalField(max_digits=10, decimal_places=4)
	SEK = models.DecimalField(max_digits=10, decimal_places=4)
	CHF = models.DecimalField(max_digits=10, decimal_places=4)
	ISK = models.DecimalField(max_digits=10, decimal_places=4)
	NOK = models.DecimalField(max_digits=10, decimal_places=4)
	HRK = models.DecimalField(max_digits=10, decimal_places=4)
	RUB = models.DecimalField(max_digits=10, decimal_places=4)
	TRY = models.DecimalField(max_digits=10, decimal_places=4)
	AUD = models.DecimalField(max_digits=10, decimal_places=4)
	BRL = models.DecimalField(max_digits=10, decimal_places=4)
	CAD = models.DecimalField(max_digits=10, decimal_places=4)
	CNY = models.DecimalField(max_digits=10, decimal_places=4)
	HKD = models.DecimalField(max_digits=10, decimal_places=4)
	IDR = models.DecimalField(max_digits=10, decimal_places=4)
	ILS = models.DecimalField(max_digits=10, decimal_places=4)
	INR = models.DecimalField(max_digits=10, decimal_places=4)
	KRW = models.DecimalField(max_digits=10, decimal_places=4)
	MXN = models.DecimalField(max_digits=10, decimal_places=4)
	MYR = models.DecimalField(max_digits=10, decimal_places=4)
	NZD = models.DecimalField(max_digits=10, decimal_places=4)
	PHP = models.DecimalField(max_digits=10, decimal_places=4)
	SGD = models.DecimalField(max_digits=10, decimal_places=4)
	THB = models.DecimalField(max_digits=10, decimal_places=4)
	ZAR = models.DecimalField(max_digits=10, decimal_places=4)


	def __str__(self):
		return f'{self.currency_date}'