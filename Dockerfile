FROM alpine:3.11.5

ARG WORKDIR=/var/lib/currency_converter/
ARG VERSION="dev"

COPY requirements.txt /tmp/.

RUN apk add --update python3 \
		     python3-dev \
		     py-pip \
		     gcc\
		     libc-dev\
		     mysql-client\
		     mariadb-dev\
		     ipython\
    && pip3 install --upgrade pip\ 
    && pip3 install --no-cache-dir -r /tmp/requirements.txt \
    && mkdir /var/lib/currency_converter

COPY source/apps /var/lib/currency_converter/apps/ 
COPY source/currency_converter /var/lib/currency_converter/currency_converter/
COPY source/manage.py /var/lib/currency_converter/

WORKDIR $WORKDIR

