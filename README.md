# currency_converter

## Prerequisites
**Docker**

**docker-compose**

**free port:8080**

## Instructions:

1. Clone project
2. Run **docker-compose up**
3. Go to [localhost:8080/convert/](localhost:8080/convert/)
4. Enter the **amount**, **src_currency**, **dest_currency** and **reference_date** as query params in the url separated by '**&**' like this:
    [localhost:8080/convert/**amount**=5555&**src_currency**=BGN&**dest_currency**=EUR&**reference_date**=2020-04-20](localhost:8080/convert/amount=5555&src_currency=BGN&dest_currency=EUR&reference_date=2020-04-20)